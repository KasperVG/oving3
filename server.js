var express = require("express");
var bodyParser = require("body-parser");
const { exec } = require("child_process");
var path = require("path");
const fs = require("fs");
var app = express();
app.use(bodyParser.json());

app.use(express.static("public"));

app.post("/", (req, res) => {
  console.log(req.body.code);
  fs.writeFile("docker_files/code.cpp", req.body.code, function(err) {
    if (err) {
      return console.log(err);
    }
    console.log("The file was saved!");
  });
  exec(
    "sudo docker build -t my-gcc-app docker_files/.",
    (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`);
        res.status(400);
        res.json({ output: error.message });
        return;
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`);
        res.status(400);
        res.json({ output: stderr });
        return;
      }
      console.log(`stdout: ${stdout}`);
      exec("sudo docker run my-gcc-app", (error, stdout, stderr) => {
        if (error) {
          console.log(`error: ${error.message}`);
          res.status(400);
          res.json({ output: error.message });
          return;
        }
        if (stderr) {
          console.log(`stderr: ${stderr}`);
          res.status(400);
          res.json({ output: stderr });
          return;
        }
        console.log(`stdout: ${stdout}`);
        res.status(200);
        res.json({ output: stdout });
        return;
      });
    }
  );
});

let port = 8080;
console.log("listening on port: " + port);
var server = app.listen(port);
